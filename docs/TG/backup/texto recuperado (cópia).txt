DutosMobile, Aplicativo móvel para visualização de dutos subterrâneos: Avaliação de tecnologias e implementação.
 
3 – AVALIAÇÕES DAS TECNOLOGIAS
 
Neste capítulo será apresentado as características que levaram a escolha das ferramentas utilizadas para o desenvolvimento do aplicativo de caso de uso, avaliando todos os softwares e frameworks disponíveis para cada um dos módulos da aplicação.
 
O principal objetivo desta avaliação é determinar qual tecnologia se encaixará nos requisitos propostos para a solução do sistema móvel de visualização de gasodutos. Todas as tecnologias serão avaliadas principalmente nos quesitos desempenho e recursos disponíveis. Ao Final as tecnologias serão selecionadas de acordo com os critérios expostos na avaliação.
 
Este capítulo está organizado da seguinte forma:
 
a) Subcapítulo 3.1 avalia todas as tecnologias utilizadas no módulo cliente, descrevendo suas funcionalidades, benefícios e pontos negativos.
 
b) Subcapítulo 3.2 avalia diversas tecnologias que poderão ser alternativas para serem utilizadas no módulo servidor da aplicação.
 
c) Subcapítulo 3.3 aborda todas as tecnologias selecionadas baseadas nas avaliações dos subcapítulos anteriores.
 
3.1 – Módulo Cliente
 
Para o módulo cliente a proposta é que ele seja apenas um visualizador de dados, logo, a leveza dos frameworks e funcionalidades para visualização de dados são essenciais.
 
3.1.1 – Bibliotecas de Interface Gráfica
 
As bibliotecas de Interface gráfica avaliadas foram: JQuery Mobile, Sencha Touch, Jo e JQTouch, os testes se basearam nas demonstrações oficiais disponíveis no site de cada uma das empresas.
 
3.1.1.1 – JQuery Mobile
 
JQuery Mobile apresenta um leque de recursos muito interessantes, a documentação do sistema é muito bem elaborada e demonstra todas as funcionalidades implementas no framework.
 
JQuery Mobile utiliza HTML5 e CSS3 para incorporar elementos visuais e eventos aos seus widgets, sua principal característica é o uso das marcações data-attributes que são elementos semânticos do HTML5 que permitem a adição de dados, eventos e efeitos baseados somente no tipo de marcação realizada (REID, 2011).   
 
Uma ferramenta de prototipação presente no site oficial do JQuery Mobile da empresa codiga, permite a rápida criação de aplicativos utilizando todos os widgets presentes no JQuery Mobile (CODIGA, 2012). O que mais chama a atenção foi a qualidade do código gerado que não utiliza componentes desnecessários, viabilizando o uso da ferramenta não só como prototipação mas como ferramenta auxiliar na produção do software. 
 


















Figura 1: Ferramenta de prototipação do JQuery Mobile
 
Além da documentação oficial o framework demonstra através de aplicativos reais o uso da ferramenta, disponível em http://www.jqmgallery.com/ todos os exemplos demonstram de forma prática as diferentes formas de se utilizar os widgets (JQUERYMOBILE, 2012), além de existir a possibilidade da inspeção do elemento para visualizar os seus comportamentos, um auxílio interessante à documentação oficial.
 
Um dos pontos negativos detectados na utilização do JQuery Mobile foi a renderização dos componentes visuais, que não apresentou um desempenho satisfatório, além de que bugs com efeitos de transições de tela também foram identificados, porém, a quantidade de widgets presentes e aparelhos que suportam este framework suprem esta deficiência que possivelmente será corrigida nas próximas versões.
 
Por fim, o JQuery Mobile apresentou-se um framework maduro e pronto para a utilização para aplicativos nativos e para web mobile, a arquitetura JQuery UI e JQuery são um complemento essencial na utilização do framework, pois possui funções de apresentação e comunicação imprescindíveis para o desenvolvimento das aplicações.
 
3.1.1.2 – Sencha Touch 2
 
Um dos recursos que apresentam um maior destaque no uso desta ferramenta são as widgets de visualização, com aproximadamente 50 componentes pré-construídos (SENCHA, 2012).
 
A experiência do usuário ao utilizar um sistema com Sencha Touch é rica e muito bem detalhada, porém, nos testes de desempenho realizados, foram detectadas lentidões no carregamento das páginas, mesmo com metodologias de redução e ofuscamento do código fonte.
 
Outro componente que chama a atenção é sua documentação, que é muito completa, apresentando os seus recursos em quatro partes:
 


















TODO: FIGURA XX: Imagem da Documentação do Sencha Touch 2 (SENCHA, 2012).
 
- Documentação da API: demonstra todos os eventos, componentes, Classes e utilidades presentes em seu framework; 
 
- Guias: tutoriais demonstram passo-a-passo sobre como uma aplicação deve ser construída. Dividida por módulos e devidamente pensada para que desenvolvedores iniciem pelo nível mais básico e evolua conforme a leitura;
 
- Vídeos: auxiliam o programador com vídeo-aulas e palestras referentes ao desenvolvimento na plataforma;
 
- Exemplos: diversos exemplos de aplicações reais que são demonstradas em um aparelho móvel virtual. Um dos pontos negativos desta demonstração é que o javascript é ofuscado, técnica que melhora o desempenho da aplicação e dificulta a leitura do código para humanos, logo, não é possível inspecionar os elementos da aplicação em tempo de execução.
 
Utilizando a arquitetura MVC o framework apresenta uma curva de aprendizado grande, pois o programador deve ter um conhecimento mais aprofundado na linguagem javascript para entender como utilizar cada um dos componentes.
 
Licenciado sob licença de Código-Aberto, o Sencha Touch é uma ótima opção para uso, porém, sua vocação é empresarial e dependendo do seu uso existe cobrança para o desenvolvimento das aplicações. A dificuldade de uso e grande curva de aprendizado podem dificultar o andamento de um projeto com uma equipe que não tenha um amplo conhecimento em javascript.
 
3.1.1.3 - Jo
 
A quantidade de componentes presentes no framework não é tão extensa em relação aos outros frameworks analisados, Jo está em fase de amadurecimento e necessita da inclusão de melhorias nos temas nativos, para que a visualização dos aplicativos sejam mais agradáveis. 
 
















Figura 2: Exemplo de Widgets no JO
 
TODO: FIGURA XX: exemplo de Widgets no JO
 
No site oficial do framework existem apenas quatro exemplos a respeito do uso dos widgets disponíveis. A documentação é bem estruturada e possui formato de livro, descrevendo o uso da API em capítulos.
 
A biblioteca é leve e nos testes realizados o desempenho foi satisfatório, porém, apesar do JO apresentar condições de uso as demais bibliotecas apresentaram uma quantidade de recursos mais interessantes e variadas.
 
3.1.1.4 - JQTouch
 
JQTouch é uma implementação do ZeptoJS, bilioteca javascript de visualização de componentes móveis, com a adição de efeitos de transição e funções do JQuery.
 
O que mais chama a atenção neste framework é sua leveza e desempenho com que a aplicação é executada no aparelho.  
 
Zepto mostrou-se a biblioteca mais leve em relação às demais pesquisadas,  ... Sua leveza aliada ao ótimo desempenho 
 
3.1.2 Bibliotecas de Visualização de Mapas
 
Nos subcapítulos à seguir, serão avaliados as tecnologias disponíveis no módulo cliente para a visualização de mapas, todas as soluções adotadas para o comparativo utilizam a linguagem de programação JavaScript e possui licença Open-Source. As escolhas pelas melhores bibliotecas baseou-se no desempenho, quantidade de recursos disponíveis e implementação dos padrões OGC. 
 
3.1.2.1 – OpenLayers
 
OpenLayers apresenta um nível de maturidade muito superior em relação à visualização de mapas.
 
3.1.2.2 – Leaflet
 
Leaflet também implementa o padrão OGC WMS de visualização de mapas, apresenta menos recursos que o OpenLayers, porém, segundo os desenvolvedores o Leaflet  não tenta fazer tudo para todos, o seu objetivo é o de fazer as coisas básicas funcionarem perfeitamente, além de ser flexível para a escrita de novos plugins (LEAFLET, 2012).  
 
A lista de componentes disponíveis no framework está descrita no site oficial, a documentação da API é interessante pois demonstra o exemplo funcionando e logo abaixo o seu respectivo código, representado na figura XX.
 




















Leaflet apresentou funcionalidades interessantes, fatores como leveza e suporte para ambientes móveis são um atrativo para o uso desta ferramenta.
 
3.2 - Módulo Servidor
 
Serão considerados como principais requisitos para a escolha da tecnologia do servidor a implementação do padrão OGC e quantidade de Bancos de dados suportados.
 
3.2.1 – GeoServer
 
A Aplicação GeoServer têm por objetivo facilitar a disponibilização de informações geográfica por qualquer usuário, de maneira rápida e objetiva (GEOSERVER, 2012). 
 
O GeoServer permite através de uma interface web a administração da fonte de dados de maneira bastante objetiva, em apenas alguns minutos é possível realizar a leitura dos dados e disponibilizá-los através de web services. 
 
Implementado na linguagem de programação Java, o GeoServer possui bom desempenho e estabilidade. Uma grande comunidade de empresas e desenvolvedores  estão ligadas ao desenvolvimento e correção desta aplicação (GEOSERVER, 2012). 
 
3.2.2 – CartoDB
 
A premissa do CartoDB é muito interessante pois sua implementação é altamente escalável, suportando milhares de requisições pois é implementado em Node.JS, tecnologia assíncrona de alto desempenho que utiliza javascript no lado servidor (NODEJS, 2012).
 
A instalação do CartoDB é complicada e depende de uma série de programas secundários. A configuração do ambiente é demorada e demanda um nível de conhecimento mais aprofundado no ambiente Linux, do qual é o único sistema operacional que permite sua implementação. 
 
Na busca da documentação do CartoDB não foi encontrado a implementação dos padrões OGC para serviços de mapas. Os exemplos de como utilizar a API e a documentação são extensos. Todo o código de cada um dos exemplos disponíveis no site oficial estão expostos logo abaixo de cada um dos exemplos, como na figura XX.


















 
Apesar do CartoDB apresentar recursos de alta escalabilidade e alto desempenho no serviços de mapas, a falta de implementação do padrão OGC é um fator que faz falta na atual implementação deste servidor de mapas.
 
3.2.3 – MapServer
 
MapServer é uma biblioteca muito madura, implementa os padrões da OGC porém o seu fim como descrito no site oficial não é o uso comercial, a principio o seu uso é acadêmico e científico (MAPSERVER, 2012).
 
MapServer possui boa documentação e diversos tutorias podem ser encontrados no site oficial. O uso da ferramenta é complexo e demanda bastante tempo de dedicação para aprendizado.
 
3.3 – Tecnologias Selecionadas
 
O bom desempenho das aplicações são essenciais para o seu sucesso, visto que a interação do usuário com o sistema, principalmente nos ambientes móveis, é de essencial importância. Lentidões foram relatadas no uso dos frameworks JQuery Mobile e Sencha Touch, os principais frameworks do mercado. Por este motivo, alternativas leves e rápidas como JO e JQTouch foram as melhores opções para uso no tipo de desenvolvimento proposto neste trabalho,
 
Considerando que a bilblioteca JO possui uma quantidade menor de widgets e efeitos visuais a melhor alternativa foi o JQTouch, que além de apresentar uma maior quantidade de widgets é extremamente leve, possuindo apenas 4KB de tamanho em sua versão minimizada, uma qualidade excelente, levando em consideração a rapidez com que a aplicação será executada.
 
No Contexto de aplicação de leitura do módulo cliente a escolha foi pelo Framework Leaflet, que apresentou um ótimo desempenho, além de possuir foco e  
 
No módulo servidor a escolha deu-se para o GeoServer, pois, a quantidade de funcionalidades e nível de maturidade da aplicação foram essenciais para a escolha. A implementações de padrões OGC como WMS, WCS e WFS-T (OGC, 2011) também foram fatores que influenciaram na escolha, pois são essenciais para interoperabilidade da aplicação quanto à publicação de dados na Web.
 
Outros serviços que destacam a escolha pelo GeoServer é a implementação das especificações WMS (Web Map Service) que permite criar mapas e exportá-los em vários formatos de saída e WFS-T (Web Feature Service Transaction) que permite o compartilhamento e edição dos mapas como inserção e exclusão dos dados (OGC, 2011). A integração do OpenLayers como visualizador padrão dos dados do GeoServer (GEOSERVER, 2012) também foi um ponto crucial na escolha por este servidor.
 
Baseado nos conceitos apresentados nos capítulos anteriores a Tabela a seguir demonstra todas as ferramentas que serão utilizadas no desenvolvimento do Software de visualização de Gasodutos:
 
TODO: CRIAR TABELA MOSTRANDO O SOFTWARE E SUA VERSÃO.
 

